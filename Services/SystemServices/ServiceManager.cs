﻿using Domain.IRepository;
using Services.Abstractions.SystemServices.Abstractions;
using System;

namespace Services.SystemServices
{
    public sealed class ServiceManager : IServiceManager
    {
        private readonly IUserService _UserService;

        public ServiceManager(IRepositoryManager RepositoryManager)
        {
            _UserService = new UserService(RepositoryManager);
        }
        public IUserService UserService => _UserService;
    }
}
