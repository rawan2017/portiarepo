﻿using Domain.IRepository;
using DTO;
using Services.Abstractions.SystemServices.Abstractions;
using System.Linq;
using System.Threading.Tasks;

namespace Services.SystemServices
{
    public class UserService : IUserService
    {
        private readonly IRepositoryManager RepositoryManager;
        public UserService(IRepositoryManager RepositoryManager)
        {

            //Test
            this.RepositoryManager = RepositoryManager;
        }
        public async Task<UserDto[]> GetUsers()
        {
            var data = await RepositoryManager.UserRepository.GetUsers();
            return data.ToList().Select(x => new UserDto()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
            }).ToArray();
        }
    }
}
