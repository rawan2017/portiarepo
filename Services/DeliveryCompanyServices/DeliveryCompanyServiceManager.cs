﻿using Domain.Enums;
using Domain.IRepository;
using Services.Abstractions.DeliveryCompanyServices.Abstractions;
using System;
using System.Collections.Generic;

namespace Services.DeliveryCompanyServices
{
    public sealed class DeliveryCompanyServiceManager : IDeliveryCompanyServiceManager
    {
        private readonly IDictionary<Enum, IDeliveryCompanyService> _AramexService = new Dictionary<Enum, IDeliveryCompanyService>();
        private readonly IDeliveryCompanyService _LyveService;

        public DeliveryCompanyServiceManager(IRepositoryManager RepositoryManager)
        {
            _AramexService.Add(Enums.Region.KSA, new KSAService(RepositoryManager));
            _AramexService.Add(Enums.Region.UAE, new UAEService(RepositoryManager));
            _LyveService = new LyveService(RepositoryManager);
              



        }
        public IDeliveryCompanyService LyveService => _LyveService;
        public IDictionary<Enum, IDeliveryCompanyService> AramexServices => _AramexService;
    }

}
