﻿using Domain.Enums;

using Services.Abstractions.DeliveryCompanyServices.Abstractions;

namespace Services
{
    public class DeliveryCompanyServiceFactory
    {
        private readonly IDeliveryCompanyServiceManager DeliveryCompanyServiceManager;
        public DeliveryCompanyServiceFactory(IDeliveryCompanyServiceManager DeliveryCompanyServiceManager)
        {
            this.DeliveryCompanyServiceManager = DeliveryCompanyServiceManager;
        }
        public IDeliveryCompanyService GetDeliveryCompanyServiceFactory(Enums.DeliveryCompany deliverycompany, Enums.Region region)
        {
            switch (deliverycompany)
            {
                case (Enums.DeliveryCompany.Aramex):
                    {
                        if (region == Enums.Region.KSA) return DeliveryCompanyServiceManager.AramexServices[Enums.Region.KSA];
                        else
                        if (region == Enums.Region.UAE) return DeliveryCompanyServiceManager.AramexServices[Enums.Region.UAE];
                        else
                            return null;
                    }
                case (Enums.DeliveryCompany.Lyve):
                    return DeliveryCompanyServiceManager.LyveService;
                default: return null;
            }
        }
    }
}
