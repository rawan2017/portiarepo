﻿using Domain.IRepository;
using Services.Abstractions.DeliveryCompanyServices.Abstractions;
using System.Threading.Tasks;

namespace Services.DeliveryCompanyServices
{
       public class LyveService : IDeliveryCompanyService
    {
        private readonly IRepositoryManager RepositoryManager;
        public LyveService(IRepositoryManager RepositoryManager)
        {
            this.RepositoryManager = RepositoryManager;
        }
        public async Task<double> GetCalculationRate()
        {
            //RepositoryManager
            return await Task.FromResult(1000.0);
        }
    }
}
