﻿using Domain.IRepository;
using Services.Abstractions.DeliveryCompanyServices.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.DeliveryCompanyServices
{
    public abstract class AramexService : IDeliveryCompanyService
    {
        private readonly IRepositoryManager RepositoryManager;
        public AramexService(IRepositoryManager RepositoryManager)
        {
            this.RepositoryManager = RepositoryManager;
        }
        public abstract Task<double> GetCalculationRate();
    }
    public class KSAService : AramexService
    {
        public KSAService(IRepositoryManager RepositoryManager) : base(RepositoryManager)
        {
        }
        public override async Task<double> GetCalculationRate()
        {
            //RepositoryManager
            return await Task.FromResult(2001.0);
        }
    }
    public class UAEService : AramexService
    {
        public UAEService(IRepositoryManager RepositoryManager) : base(RepositoryManager)
        {
        }
        public override async Task<double> GetCalculationRate()
        {
            //RepositoryManager
            return await Task.FromResult(2003.0);
        }
    }
}
