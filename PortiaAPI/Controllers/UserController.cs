﻿using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Abstractions.SystemServices.Abstractions;
using System.Threading.Tasks;
namespace PortiaAPI.Controllers
{
    [Route("api/user")]
    [ApiController]

    //[Route("api/v{version:apiVersion}/[controller]")]
    //[ApiController]
    //[ApiVersion("1.0")]
    public class UserController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public UserController(IServiceManager serviceManager) => _serviceManager = serviceManager;

        // GET: api/<UserController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var accountsDto = await _serviceManager.UserService.GetUsers();
            return Ok(accountsDto);
        }
    }
}
