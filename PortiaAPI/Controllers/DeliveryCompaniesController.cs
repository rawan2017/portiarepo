﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PortiaAPI.Common;
using Services;
using Services.Abstractions;
using Services.Abstractions.DeliveryCompanyServices.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace PortiaAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DeliveryCompaniesController : ControllerBase
    {
        private readonly DeliveryCompanyServiceFactory DeliveryCompanyServiceFactory;
        private readonly ILogger<DeliveryCompaniesController> _logger;
        public DeliveryCompaniesController(ILogger<DeliveryCompaniesController> logger, IDeliveryCompanyServiceManager DeliveryCompanyServiceManager)
        {
            _logger = logger;
            this.DeliveryCompanyServiceFactory = new DeliveryCompanyServiceFactory(DeliveryCompanyServiceManager);
        }
        [HttpPost]
        public async Task<IActionResult> GetCalculationRate([Required] Domain.Enums.Enums.DeliveryCompany deliverycompany, [Required] Domain.Enums.Enums.Region region )
        {
            ApiResponse response = new ApiResponse(StatusCodes.Status200OK.ToString());
            try
            {
                var service = DeliveryCompanyServiceFactory.GetDeliveryCompanyServiceFactory(deliverycompany, region);
                response.status = StatusCodes.Status200OK.ToString();
                response.data = await service.GetCalculationRate();
                response.message = ("success");
            }
            catch (WebException ex)
            {
                HttpWebResponse error = (HttpWebResponse)ex.Response;
                response.status = error.StatusCode.ToString();
                response.data = null;
                response.message = ex.Message;
                //_logger.LogError($"Error Message : {ex.Message}");
            }
            catch (Exception ex)
            {
                response.status = ex.HResult.ToString();
                response.data = null;
                response.message = ex.Message;
            }

         

            return Ok(response);
        }
    }
}
