﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortiaAPI.Common
{
    public class ApiMessages
    {
        public static string NotFound = "No data is found";
        public static string BadRequest = "Bad Request";
        public static string InternalServerError = "Internal Server Error";
        public static string Ok = "Success";
    }
}
