﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortiaAPI.Common
{

    public class ApiResponse
    {
        public ApiResponse(string statusCode, object dataRes = null, string msgRes = null)
        {
            switch (statusCode)
            {
                case "200":
                    this.status = "200";
                    this.message = msgRes is null ? ApiMessages.Ok : msgRes;
                    this.data = dataRes;
                    break;
                case "400":
                    this.status = "400";
                    this.message = msgRes is null ? ApiMessages.BadRequest : msgRes;
                    this.data = dataRes;
                    break;
                case "404":
                    this.status = "404";
                    this.message = msgRes is null ? ApiMessages.NotFound : msgRes;
                    this.data = dataRes;
                    break;
                case "500":
                    this.status = "500";
                    this.message = msgRes is null ? ApiMessages.InternalServerError : msgRes;
                    this.data = dataRes;
                    break;

                default:
                    this.status = statusCode;
                    this.message = msgRes is null ? "" : msgRes;
                    this.data = dataRes;
                    break;
            }
        }
        public string status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
   
}
