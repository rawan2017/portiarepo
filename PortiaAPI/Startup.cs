using Amazon.S3;
using Domain.IRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Persistence.Data;
using Persistence.Repositories;
using PortiaAPI.Extensions;
using PortiaAPI.Middleware;
using Services;
using Services.Abstractions;
using Services.Abstractions.DeliveryCompanyServices.Abstractions;
using Services.Abstractions.SystemServices.Abstractions;
using Services.DeliveryCompanyServices;
using Services.SystemServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortiaAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          
            services.AddControllers();
            services.AddScoped<IServiceManager, ServiceManager>();
            services.AddScoped<IDeliveryCompanyServiceManager, DeliveryCompanyServiceManager>();
           
            services.AddAWSService<IAmazonS3>(Configuration.GetAWSOptions());

            services.AddScoped<IRepositoryManager, RepositoryManager>();
            services.AddDbContext<portiadbtestingContext>(options => options.UseMySql("server=localhost;database=portia-db-testing;user=root;password=Password2020", x => x.ServerVersion("8.0.21-mysql")));

            services.AddMvc().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
            });
            services.AddSwaggerGenNewtonsoftSupport();
            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Portai .Net API",
                    Version = "v1",
                    Description = ""
                });
            });

            services.AddApiVersioning(setup =>
            {
                setup.DefaultApiVersion = new ApiVersion(1, 0);
                setup.AssumeDefaultVersionWhenUnspecified = true;
                setup.ReportApiVersions = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseLoggingMiddleware();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Portai .Net API");
            });

        }
    }
}
