﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace PortiaAPI.Middleware
{
    public class RequestResponseLoggingMiddleware
    {
        private readonly ILogger<RequestResponseLoggingMiddleware> logger;
        private readonly RequestDelegate _next;
        public RequestResponseLoggingMiddleware(RequestDelegate next, ILogger<RequestResponseLoggingMiddleware> logger)
        {
            this.logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
       
            //var originalBody = httpContext.Response.Body;
            //var newBody = new MemoryStream();
            //httpContext.Response.Body = newBody;
            try
            {
                logger.LogInformation($"Http Request Information:{Environment.NewLine}" +
                                       $"Schema:{httpContext.Request.Scheme} " +
                                       $"Host: {httpContext.Request.Host} " +
                                       $"Path: {httpContext.Request.Path} " +
                                       $"QueryString: {httpContext.Request.QueryString} "
                                       );
                await _next(httpContext);
            }
            finally
            {
                //newBody.Seek(0, SeekOrigin.Begin);
                //var bodyText = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();
                //logger.LogInformation($"Http Request Information:{Environment.NewLine}" +
                //                       $"Schema:{httpContext.Request.Scheme} " +
                //                       $"Host: {httpContext.Request.Host} " +
                //                       $"Path: {httpContext.Request.Path} " +
                //                       $"QueryString: {httpContext.Request.QueryString} " +
                //                       $"Request Response: {bodyText}"
                //                       );
                //newBody.Seek(0, SeekOrigin.Begin);
                //await newBody.CopyToAsync(originalBody);
            }

             

        }
 
    }

}
 