using System;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

using Serilog;
using Serilog.Sinks.Elasticsearch;
using Serilog.Exceptions;
using Amazon.Runtime;
using AWS.Logger;
using Serilog.Formatting.Json;
using AWS.Logger.SeriLog;

namespace PortiaAPI
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
             .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
             .Build();


            /*****************Amazon Log********************/
            // you can get these configs from your appsettings.json 
            var awsS3sKey = configuration["aws:access.key"];
            var awsS3Secret = configuration["aws:secret.key"];
            var logGroupName = configuration["aws:log.group.name"];
            var region = configuration["aws:log.region"];

            // create your AWS credential
            var credentials = new BasicAWSCredentials(awsS3sKey, awsS3Secret);

            // create a logger for AWS Cloudwatch
            var awsConfiguration = new AWSLoggerConfig
            {
                Region = region,
                LogGroup = logGroupName,
                Credentials = credentials
            };

     
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .WriteTo.AWSSeriLog(awsConfiguration, null, new JsonFormatter()).CreateLogger();


            /*****************Elastic Sreach Log********************/

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft.AspNetCore", Serilog.Events.LogEventLevel.Warning)
              //  .Enrich.WithElasticApmCorrelationInfo()
                 .Enrich.FromLogContext()
                //.Enrich.WithExceptionDetails()
                .Enrich.WithMachineName()
                //.WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(configuration["ElasticSearchUrl"]))
                {
                    AutoRegisterTemplate = true,
                    AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv6
                })
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
             //   Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
    }
}

 
