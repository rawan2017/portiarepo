﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class UserDto
    {
        public ulong Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
