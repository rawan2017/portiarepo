﻿using Domain.IRepository;
using Persistence.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    internal sealed class UnitOfWork : IUnitOfWork
    {
        private readonly portiadbtestingContext _dbContext;

        public UnitOfWork(portiadbtestingContext dbContext) => _dbContext = dbContext;

        public void SaveChangesAsync()
        {
            _dbContext.SaveChangesAsync();
        }
       
    }
}
