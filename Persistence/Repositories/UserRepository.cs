﻿ 
using Domain.IRepository;
using Microsoft.EntityFrameworkCore;
using Persistence.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{

    internal sealed class UserRepository : IUserRepository
    {
        private readonly portiadbtestingContext _dbContext;

        public UserRepository(portiadbtestingContext dbContext) => _dbContext = dbContext;
        public async Task<Users[]> GetUsers() =>
            await _dbContext.Users.Take(5).ToArrayAsync();
    }
}
