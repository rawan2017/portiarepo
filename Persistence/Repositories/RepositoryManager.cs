﻿using Domain.IRepository;
using Persistence.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Repositories
{
    public sealed class RepositoryManager : IRepositoryManager
    {
        private readonly Lazy<IUserRepository> _UserRepository;
        private readonly Lazy<IUnitOfWork> _UnitOfWork;

        public RepositoryManager(portiadbtestingContext dbContext)
        {
            _UserRepository = new Lazy<IUserRepository>(() => new UserRepository(dbContext));
            _UnitOfWork = new Lazy<IUnitOfWork>(() => new UnitOfWork(dbContext));
        }
        public IUserRepository UserRepository => _UserRepository.Value;
        public IUnitOfWork UnitOfWork => _UnitOfWork.Value;
    }
}
