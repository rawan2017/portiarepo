﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.IRepository
{
   public interface IRepositoryManager
    {
        IUserRepository UserRepository { get; }
        IUnitOfWork UnitOfWork { get; }
    }
}
