﻿using Persistence; // Temporary , it should be renamed by command 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IUserRepository
    {
        Task<Users[]> GetUsers();
        //void CreateUser(User user);
    }
}
