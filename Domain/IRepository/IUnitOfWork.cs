﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
   public interface IUnitOfWork
    {
       void SaveChangesAsync();
    }
}
