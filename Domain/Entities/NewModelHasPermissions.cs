﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("new_model_has_permissions")]
    public partial class NewModelHasPermissions
    {
        [Key]
        [Column("permission_id")]
        public uint PermissionId { get; set; }
        [Key]
        [Column("model_type", TypeName = "varchar(255)")]
        public string ModelType { get; set; }
        [Key]
        [Column("model_id")]
        public ulong ModelId { get; set; }

        [ForeignKey(nameof(PermissionId))]
        [InverseProperty(nameof(NewPermissions.NewModelHasPermissions))]
        public virtual NewPermissions Permission { get; set; }
    }
}
