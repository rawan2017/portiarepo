﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("order_purchases")]
    public partial class OrderPurchases
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("product_name_en", TypeName = "text")]
        public string ProductNameEn { get; set; }
        [Column("product_name_ar", TypeName = "text")]
        public string ProductNameAr { get; set; }
        [Column("quantity")]
        public int Quantity { get; set; }
        [Column("image_url", TypeName = "varchar(300)")]
        public string ImageUrl { get; set; }
        [Column("attributes", TypeName = "text")]
        public string Attributes { get; set; }
        [Column("unit_price_amount", TypeName = "decimal(12,3)")]
        public decimal UnitPriceAmount { get; set; }
        [Column("portia_order_id")]
        public long PortiaOrderId { get; set; }
        [Required]
        [Column("vetrina_order_id", TypeName = "varchar(200)")]
        public string VetrinaOrderId { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Column("sku", TypeName = "text")]
        public string Sku { get; set; }
        [Column("custom_fields", TypeName = "text")]
        public string CustomFields { get; set; }
        [Column("charges", TypeName = "text")]
        public string Charges { get; set; }
        [Column("total", TypeName = "decimal(12,3)")]
        public decimal? Total { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("dimensions", TypeName = "text")]
        public string Dimensions { get; set; }
    }
}
