﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("admin_tenants")]
    public partial class AdminTenants
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("tenant_id")]
        public ulong TenantId { get; set; }
        [Column("admin_id")]
        public ulong AdminId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(AdminId))]
        [InverseProperty(nameof(Admins.AdminTenants))]
        public virtual Admins Admin { get; set; }
    }
}
