﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("role_user")]
    public partial class RoleUser
    {
        [Key]
        [Column("role_id")]
        public ulong RoleId { get; set; }
        [Key]
        [Column("user_id")]
        public ulong UserId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty(nameof(Roles.RoleUser))]
        public virtual Roles Role { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(Admins.RoleUser))]
        public virtual Admins User { get; set; }
    }
}
