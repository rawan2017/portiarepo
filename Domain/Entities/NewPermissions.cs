﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("new_permissions")]
    public partial class NewPermissions
    {
        public NewPermissions()
        {
            NewModelHasPermissions = new HashSet<NewModelHasPermissions>();
            NewRoleHasPermissions = new HashSet<NewRoleHasPermissions>();
        }

        [Key]
        [Column("id")]
        public uint Id { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column("group", TypeName = "varchar(255)")]
        public string Group { get; set; }
        [Required]
        [Column("guard_name", TypeName = "varchar(255)")]
        public string GuardName { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [InverseProperty("Permission")]
        public virtual ICollection<NewModelHasPermissions> NewModelHasPermissions { get; set; }
        [InverseProperty("Permission")]
        public virtual ICollection<NewRoleHasPermissions> NewRoleHasPermissions { get; set; }
    }
}
