﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("service_tokens")]
    public partial class ServiceTokens
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("service_name", TypeName = "varchar(255)")]
        public string ServiceName { get; set; }
        [Required]
        [Column("access_token", TypeName = "text")]
        public string AccessToken { get; set; }
        [Column("revoked")]
        public sbyte Revoked { get; set; }
        [Column("expires_at", TypeName = "datetime")]
        public DateTime ExpiresAt { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
