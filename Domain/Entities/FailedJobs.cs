﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("failed_jobs")]
    public partial class FailedJobs
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("uuid", TypeName = "varchar(255)")]
        public string Uuid { get; set; }
        [Required]
        [Column("connection", TypeName = "text")]
        public string Connection { get; set; }
        [Required]
        [Column("queue", TypeName = "text")]
        public string Queue { get; set; }
        [Required]
        [Column("payload", TypeName = "longtext")]
        public string Payload { get; set; }
        [Required]
        [Column("exception", TypeName = "longtext")]
        public string Exception { get; set; }
        [Column("failed_at", TypeName = "timestamp")]
        public DateTime FailedAt { get; set; }
    }
}
