﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("telescope_entries")]
    public partial class TelescopeEntries
    {
        [Key]
        [Column("sequence")]
        public ulong Sequence { get; set; }
        [Column("uuid")]
        public Guid Uuid { get; set; }
        [Column("batch_id")]
        public Guid BatchId { get; set; }
        [Column("family_hash", TypeName = "varchar(255)")]
        public string FamilyHash { get; set; }
        [Required]
        [Column("should_display_on_index")]
        public bool? ShouldDisplayOnIndex { get; set; }
        [Required]
        [Column("type", TypeName = "varchar(20)")]
        public string Type { get; set; }
        [Required]
        [Column("content", TypeName = "longtext")]
        public string Content { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
    }
}
