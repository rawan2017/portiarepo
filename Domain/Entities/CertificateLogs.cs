﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("certificate_logs")]
    public partial class CertificateLogs
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Column("domain", TypeName = "varchar(255)")]
        public string Domain { get; set; }
        [Column("type", TypeName = "varchar(255)")]
        public string Type { get; set; }
        [Column("request", TypeName = "text")]
        public string Request { get; set; }
        [Column("errors", TypeName = "text")]
        public string Errors { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
