﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("quiqup_zones")]
    public partial class QuiqupZones
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("region_id")]
        public ulong? RegionId { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column("code", TypeName = "varchar(255)")]
        public string Code { get; set; }
        [Required]
        [Column("variant", TypeName = "varchar(255)")]
        public string Variant { get; set; }
        [Column("geo_type", TypeName = "varchar(255)")]
        public string GeoType { get; set; }
        [Column("coordinates", TypeName = "text")]
        public string Coordinates { get; set; }
        [Column("synced_at", TypeName = "datetime")]
        public DateTime SyncedAt { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
