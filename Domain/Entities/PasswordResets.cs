﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("password_resets")]
    public partial class PasswordResets
    {
        [Required]
        [Column("email", TypeName = "varchar(255)")]
        public string Email { get; set; }
        [Required]
        [Column("token", TypeName = "varchar(255)")]
        public string Token { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
    }
}
