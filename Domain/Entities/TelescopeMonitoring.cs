﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("telescope_monitoring")]
    public partial class TelescopeMonitoring
    {
        [Required]
        [Column("tag", TypeName = "varchar(255)")]
        public string Tag { get; set; }
    }
}
