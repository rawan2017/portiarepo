﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("new_role_has_permissions")]
    public partial class NewRoleHasPermissions
    {
        [Key]
        [Column("permission_id")]
        public uint PermissionId { get; set; }
        [Key]
        [Column("role_id")]
        public uint RoleId { get; set; }

        [ForeignKey(nameof(PermissionId))]
        [InverseProperty(nameof(NewPermissions.NewRoleHasPermissions))]
        public virtual NewPermissions Permission { get; set; }
        [ForeignKey(nameof(RoleId))]
        [InverseProperty(nameof(NewRoles.NewRoleHasPermissions))]
        public virtual NewRoles Role { get; set; }
    }
}
