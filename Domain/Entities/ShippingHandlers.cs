﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("shipping_handlers")]
    public partial class ShippingHandlers
    {
        public ShippingHandlers()
        {
            CityMappings = new HashSet<CityMappings>();
        }

        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("name_en", TypeName = "varchar(255)")]
        public string NameEn { get; set; }
        [Required]
        [Column("name_ar", TypeName = "varchar(255)")]
        public string NameAr { get; set; }
        [Column("country", TypeName = "varchar(255)")]
        public string Country { get; set; }
        [Column("token", TypeName = "varchar(255)")]
        public string Token { get; set; }
        [Column("class_name", TypeName = "varchar(255)")]
        public string ClassName { get; set; }
        [Column("currency", TypeName = "varchar(255)")]
        public string Currency { get; set; }
        [Column("type", TypeName = "varchar(50)")]
        public string Type { get; set; }
        [Column("static_rate")]
        public sbyte? StaticRate { get; set; }
        [Column("matrix_rate", TypeName = "text")]
        public string MatrixRate { get; set; }
        [Column("restrictions", TypeName = "text")]
        public string Restrictions { get; set; }
        [Column("start_time", TypeName = "timestamp")]
        public DateTime? StartTime { get; set; }
        [Column("end_time", TypeName = "timestamp")]
        public DateTime? EndTime { get; set; }
        [Column("expires_at", TypeName = "datetime")]
        public DateTime? ExpiresAt { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [InverseProperty("Handler")]
        public virtual ICollection<CityMappings> CityMappings { get; set; }
    }
}
