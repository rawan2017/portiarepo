﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("hourly_reports")]
    public partial class HourlyReports
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("tenant_id")]
        public ulong TenantId { get; set; }
        [Column("date", TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("data", TypeName = "varchar(255)")]
        public string Data { get; set; }
        [Column("report_id", TypeName = "varchar(255)")]
        public string ReportId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
