﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("oauth_clients")]
    public partial class OauthClients
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("user_id")]
        public ulong? UserId { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Column("secret", TypeName = "varchar(100)")]
        public string Secret { get; set; }
        [Column("provider", TypeName = "varchar(255)")]
        public string Provider { get; set; }
        [Required]
        [Column("redirect", TypeName = "text")]
        public string Redirect { get; set; }
        [Column("personal_access_client")]
        public bool PersonalAccessClient { get; set; }
        [Column("password_client")]
        public bool PasswordClient { get; set; }
        [Column("revoked")]
        public bool Revoked { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
