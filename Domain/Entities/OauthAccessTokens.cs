﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("oauth_access_tokens")]
    public partial class OauthAccessTokens
    {
        [Key]
        [Column("id", TypeName = "varchar(100)")]
        public string Id { get; set; }
        [Column("user_id")]
        public ulong? UserId { get; set; }
        [Column("client_id")]
        public ulong ClientId { get; set; }
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Column("scopes", TypeName = "text")]
        public string Scopes { get; set; }
        [Column("revoked")]
        public bool Revoked { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("expires_at", TypeName = "datetime")]
        public DateTime? ExpiresAt { get; set; }
    }
}
