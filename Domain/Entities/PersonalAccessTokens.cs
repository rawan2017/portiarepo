﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("personal_access_tokens")]
    public partial class PersonalAccessTokens
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("tokenable_type", TypeName = "varchar(255)")]
        public string TokenableType { get; set; }
        [Column("tokenable_id")]
        public ulong TokenableId { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column("token", TypeName = "varchar(64)")]
        public string Token { get; set; }
        [Column("abilities", TypeName = "text")]
        public string Abilities { get; set; }
        [Column("last_used_at", TypeName = "timestamp")]
        public DateTime? LastUsedAt { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
