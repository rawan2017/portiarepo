﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("new_model_has_roles")]
    public partial class NewModelHasRoles
    {
        [Key]
        [Column("role_id")]
        public uint RoleId { get; set; }
        [Key]
        [Column("model_type", TypeName = "varchar(255)")]
        public string ModelType { get; set; }
        [Key]
        [Column("model_id")]
        public ulong ModelId { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty(nameof(NewRoles.NewModelHasRoles))]
        public virtual NewRoles Role { get; set; }
    }
}
