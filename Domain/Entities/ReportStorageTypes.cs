﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("report_storage_types")]
    public partial class ReportStorageTypes
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Column("duration", TypeName = "varchar(255)")]
        public string Duration { get; set; }
        [Required]
        [Column("scheduled_at", TypeName = "varchar(255)")]
        public string ScheduledAt { get; set; }
        [Required]
        [Column("model", TypeName = "varchar(255)")]
        public string Model { get; set; }
        [Required]
        [Column("start_timestamp", TypeName = "varchar(255)")]
        public string StartTimestamp { get; set; }
        [Required]
        [Column("generating_job", TypeName = "varchar(255)")]
        public string GeneratingJob { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
