﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("settings_histories")]
    public partial class SettingsHistories
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("orders_enabled")]
        public bool OrdersEnabled { get; set; }
        [Column("opening_times", TypeName = "text")]
        public string OpeningTimes { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
    }
}
