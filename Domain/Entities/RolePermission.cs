﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("role_permission")]
    public partial class RolePermission
    {
        [Key]
        [Column("role_id")]
        public ulong RoleId { get; set; }
        [Key]
        [Column("permission_slug", TypeName = "varchar(255)")]
        public string PermissionSlug { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty(nameof(Roles.RolePermission))]
        public virtual Roles Role { get; set; }
    }
}
