﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("sessions")]
    public partial class Sessions
    {
        [Key]
        [Column("id", TypeName = "varchar(255)")]
        public string Id { get; set; }
        [Column("user_id")]
        public ulong? UserId { get; set; }
        [Column("ip_address", TypeName = "varchar(45)")]
        public string IpAddress { get; set; }
        [Column("user_agent", TypeName = "text")]
        public string UserAgent { get; set; }
        [Required]
        [Column("payload", TypeName = "text")]
        public string Payload { get; set; }
        [Column("last_activity")]
        public int LastActivity { get; set; }
    }
}
