﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("admins")]
    public partial class Admins
    {
        public Admins()
        {
            AdminTenants = new HashSet<AdminTenants>();
            RoleUser = new HashSet<RoleUser>();
            SettingsAdmin = new HashSet<Settings>();
            SettingsOnboardingOfficer = new HashSet<Settings>();
        }

        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column("email", TypeName = "varchar(255)")]
        public string Email { get; set; }
        [Column("email_verified_at", TypeName = "timestamp")]
        public DateTime? EmailVerifiedAt { get; set; }
        [Required]
        [Column("password", TypeName = "varchar(255)")]
        public string Password { get; set; }
        [Column("remember_token", TypeName = "varchar(100)")]
        public string RememberToken { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("deleted_at", TypeName = "timestamp")]
        public DateTime? DeletedAt { get; set; }

        [InverseProperty("Admin")]
        public virtual ICollection<AdminTenants> AdminTenants { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<RoleUser> RoleUser { get; set; }
        [InverseProperty(nameof(Settings.Admin))]
        public virtual ICollection<Settings> SettingsAdmin { get; set; }
        [InverseProperty(nameof(Settings.OnboardingOfficer))]
        public virtual ICollection<Settings> SettingsOnboardingOfficer { get; set; }
    }
}
