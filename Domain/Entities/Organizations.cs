﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("organizations")]
    public partial class Organizations
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("name_ar", TypeName = "varchar(255)")]
        public string NameAr { get; set; }
        [Column("name_en", TypeName = "varchar(255)")]
        public string NameEn { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
