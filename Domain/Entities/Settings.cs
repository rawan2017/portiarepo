﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("settings")]
    public partial class Settings
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("orders_enabled")]
        public bool OrdersEnabled { get; set; }
        [Column("tenant_id")]
        public long TenantId { get; set; }
        [Column("admin_id")]
        public ulong? AdminId { get; set; }
        [Column("onboarding_officer_id")]
        public ulong? OnboardingOfficerId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime CreatedAt { get; set; }
        [Column("opening_times", TypeName = "text")]
        public string OpeningTimes { get; set; }
        [Column("pickup_address", TypeName = "text")]
        public string PickupAddress { get; set; }
        [Column("address_error_notified")]
        public sbyte AddressErrorNotified { get; set; }
        [Column("pickup_zone_id")]
        public ulong? PickupZoneId { get; set; }
        [Column("delivery_handler", TypeName = "varchar(255)")]
        public string DeliveryHandler { get; set; }
        [Column("delivery_enabled")]
        public sbyte DeliveryEnabled { get; set; }
        [Column("delivery_period")]
        public int? DeliveryPeriod { get; set; }
        [Column("should_get_delivery_time")]
        public bool? ShouldGetDeliveryTime { get; set; }
        [Column("preparation_time", TypeName = "varchar(255)")]
        public string PreparationTime { get; set; }
        [Column("timezone")]
        public int? Timezone { get; set; }
        [Required]
        [Column("matrix_rate_enabled")]
        public bool? MatrixRateEnabled { get; set; }
        [Required]
        [Column("plan", TypeName = "varchar(50)")]
        public string Plan { get; set; }
        [Column("delivering_alone")]
        public bool DeliveringAlone { get; set; }
        [Required]
        [Column("theme", TypeName = "varchar(50)")]
        public string Theme { get; set; }
        [Column("linked_domain", TypeName = "varchar(255)")]
        public string LinkedDomain { get; set; }
        [Column("is_sub_domain")]
        public bool IsSubDomain { get; set; }

        [ForeignKey(nameof(AdminId))]
        [InverseProperty(nameof(Admins.SettingsAdmin))]
        public virtual Admins Admin { get; set; }
        [ForeignKey(nameof(OnboardingOfficerId))]
        [InverseProperty(nameof(Admins.SettingsOnboardingOfficer))]
        public virtual Admins OnboardingOfficer { get; set; }
    }
}
