﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("notifications")]
    public partial class Notifications
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        [Required]
        [Column("type", TypeName = "varchar(255)")]
        public string Type { get; set; }
        [Required]
        [Column("notifiable_type", TypeName = "varchar(255)")]
        public string NotifiableType { get; set; }
        [Column("notifiable_id")]
        public ulong NotifiableId { get; set; }
        [Required]
        [Column("data", TypeName = "text")]
        public string Data { get; set; }
        [Column("read_at", TypeName = "timestamp")]
        public DateTime? ReadAt { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
