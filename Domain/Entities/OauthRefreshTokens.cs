﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("oauth_refresh_tokens")]
    public partial class OauthRefreshTokens
    {
        [Key]
        [Column("id", TypeName = "varchar(100)")]
        public string Id { get; set; }
        [Required]
        [Column("access_token_id", TypeName = "varchar(100)")]
        public string AccessTokenId { get; set; }
        [Column("revoked")]
        public bool Revoked { get; set; }
        [Column("expires_at", TypeName = "datetime")]
        public DateTime? ExpiresAt { get; set; }
    }
}
