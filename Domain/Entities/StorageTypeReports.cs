﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("storage_type_reports")]
    public partial class StorageTypeReports
    {
        [Column("report_id")]
        public ulong ReportId { get; set; }
        [Column("report_storage_type_id")]
        public ulong ReportStorageTypeId { get; set; }
    }
}
