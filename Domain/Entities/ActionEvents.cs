﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("action_events")]
    public partial class ActionEvents
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("batch_id")]
        public Guid BatchId { get; set; }
        [Column("user_id")]
        public ulong UserId { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column("actionable_type", TypeName = "varchar(255)")]
        public string ActionableType { get; set; }
        [Column("actionable_id")]
        public ulong ActionableId { get; set; }
        [Required]
        [Column("target_type", TypeName = "varchar(255)")]
        public string TargetType { get; set; }
        [Column("target_id")]
        public ulong TargetId { get; set; }
        [Required]
        [Column("model_type", TypeName = "varchar(255)")]
        public string ModelType { get; set; }
        [Column("model_id")]
        public ulong? ModelId { get; set; }
        [Required]
        [Column("fields", TypeName = "text")]
        public string Fields { get; set; }
        [Required]
        [Column("status", TypeName = "varchar(25)")]
        public string Status { get; set; }
        [Required]
        [Column("exception", TypeName = "text")]
        public string Exception { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("original", TypeName = "mediumtext")]
        public string Original { get; set; }
        [Column("changes", TypeName = "mediumtext")]
        public string Changes { get; set; }
    }
}
