﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("app_versions")]
    public partial class AppVersions
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("version", TypeName = "varchar(255)")]
        public string Version { get; set; }
        [Required]
        [Column("platform", TypeName = "enum('ios','android')")]
        public string Platform { get; set; }
        [Column("release_date", TypeName = "date")]
        public DateTime ReleaseDate { get; set; }
        [Column("minimum_version", TypeName = "varchar(20)")]
        public string MinimumVersion { get; set; }
        [Column("maximum_version", TypeName = "varchar(20)")]
        public string MaximumVersion { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
