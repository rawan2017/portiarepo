﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("report_date_ranges")]
    public partial class ReportDateRanges
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("name_en", TypeName = "varchar(255)")]
        public string NameEn { get; set; }
        [Column("name_ar", TypeName = "varchar(255)")]
        public string NameAr { get; set; }
        [Column("user_provided_start_date")]
        public bool UserProvidedStartDate { get; set; }
        [Column("user_provided_start_label", TypeName = "varchar(255)")]
        public string UserProvidedStartLabel { get; set; }
        [Column("starts_before_unit", TypeName = "varchar(255)")]
        public string StartsBeforeUnit { get; set; }
        [Column("starts_before_value")]
        public uint? StartsBeforeValue { get; set; }
        [Column("start_timestamp_format", TypeName = "varchar(255)")]
        public string StartTimestampFormat { get; set; }
        [Column("user_provided_end_date")]
        public bool UserProvidedEndDate { get; set; }
        [Column("user_provided_end_date_label", TypeName = "varchar(255)")]
        public string UserProvidedEndDateLabel { get; set; }
        [Column("ends_before_unit", TypeName = "varchar(255)")]
        public string EndsBeforeUnit { get; set; }
        [Column("ends_before_value")]
        public uint? EndsBeforeValue { get; set; }
        [Column("end_timestamp_format", TypeName = "varchar(255)")]
        public string EndTimestampFormat { get; set; }
        [Column("conditional_start_date")]
        public bool ConditionalStartDate { get; set; }
        [Column("report_storage_type_id")]
        public ulong ReportStorageTypeId { get; set; }
        [Column("has_growth")]
        public bool HasGrowth { get; set; }
        [Column("previous_period_unit", TypeName = "varchar(255)")]
        public string PreviousPeriodUnit { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
