﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("new_roles")]
    public partial class NewRoles
    {
        public NewRoles()
        {
            NewModelHasRoles = new HashSet<NewModelHasRoles>();
            NewRoleHasPermissions = new HashSet<NewRoleHasPermissions>();
        }

        [Key]
        [Column("id")]
        public uint Id { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column("guard_name", TypeName = "varchar(255)")]
        public string GuardName { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<NewModelHasRoles> NewModelHasRoles { get; set; }
        [InverseProperty("Role")]
        public virtual ICollection<NewRoleHasPermissions> NewRoleHasPermissions { get; set; }
    }
}
