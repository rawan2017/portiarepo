﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("faqs")]
    public partial class Faqs
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("body_ar", TypeName = "varchar(255)")]
        public string BodyAr { get; set; }
        [Required]
        [Column("body_en", TypeName = "varchar(255)")]
        public string BodyEn { get; set; }
        [Column("order")]
        public uint Order { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
