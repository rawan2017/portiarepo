﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("oauth_auth_codes")]
    public partial class OauthAuthCodes
    {
        [Key]
        [Column("id", TypeName = "varchar(100)")]
        public string Id { get; set; }
        [Column("user_id")]
        public ulong UserId { get; set; }
        [Column("client_id")]
        public ulong ClientId { get; set; }
        [Column("scopes", TypeName = "text")]
        public string Scopes { get; set; }
        [Column("revoked")]
        public bool Revoked { get; set; }
        [Column("expires_at", TypeName = "datetime")]
        public DateTime? ExpiresAt { get; set; }
    }
}
