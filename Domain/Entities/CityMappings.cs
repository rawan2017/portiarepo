﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("city_mappings")]
    public partial class CityMappings
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("city_label", TypeName = "varchar(255)")]
        public string CityLabel { get; set; }
        [Required]
        [Column("city_value", TypeName = "varchar(255)")]
        public string CityValue { get; set; }
        [Column("data", TypeName = "text")]
        public string Data { get; set; }
        [Column("handler_id")]
        public ulong HandlerId { get; set; }

        [ForeignKey(nameof(HandlerId))]
        [InverseProperty(nameof(ShippingHandlers.CityMappings))]
        public virtual ShippingHandlers Handler { get; set; }
    }
}
