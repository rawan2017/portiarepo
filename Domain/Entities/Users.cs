﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("users")]
    public partial class Users
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("first_name", TypeName = "varchar(255)")]
        public string FirstName { get; set; }
        [Column("last_name", TypeName = "varchar(255)")]
        public string LastName { get; set; }
        [Column("phone", TypeName = "varchar(255)")]
        public string Phone { get; set; }
        [Required]
        [Column("email", TypeName = "varchar(255)")]
        public string Email { get; set; }
        [Column("email_verified_at", TypeName = "timestamp")]
        public DateTime? EmailVerifiedAt { get; set; }
        [Required]
        [Column("password", TypeName = "varchar(255)")]
        public string Password { get; set; }
        [Column("is_vetrina")]
        public bool IsVetrina { get; set; }
        [Column("vetrina_user_id", TypeName = "varchar(255)")]
        public string VetrinaUserId { get; set; }
        [Column("vetrina_tenant_id", TypeName = "varchar(255)")]
        public string VetrinaTenantId { get; set; }
        [Column("vetrina_token", TypeName = "varchar(255)")]
        public string VetrinaToken { get; set; }
        [Column("remember_token", TypeName = "varchar(100)")]
        public string RememberToken { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
