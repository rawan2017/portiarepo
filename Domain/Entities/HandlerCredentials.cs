﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("handler_credentials")]
    public partial class HandlerCredentials
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("oto", TypeName = "longtext")]
        public string Oto { get; set; }
        [Column("yallow", TypeName = "longtext")]
        public string Yallow { get; set; }
        [Column("ksayallow", TypeName = "longtext")]
        public string Ksayallow { get; set; }
        [Column("shouroq", TypeName = "longtext")]
        public string Shouroq { get; set; }
        [Column("quiqup", TypeName = "longtext")]
        public string Quiqup { get; set; }
        [Column("aramex", TypeName = "longtext")]
        public string Aramex { get; set; }
        [Column("naqel", TypeName = "longtext")]
        public string Naqel { get; set; }
        [Column("tenant_id")]
        public ulong? TenantId { get; set; }
        [Column("oto-active")]
        public sbyte? OtoActive { get; set; }
        [Column("yallow-active")]
        public sbyte? YallowActive { get; set; }
        [Column("ksayallow-active")]
        public bool KsayallowActive { get; set; }
        [Column("shouroq-active")]
        public sbyte? ShouroqActive { get; set; }
        [Required]
        [Column("can_pick")]
        public bool? CanPick { get; set; }
        [Column("quiqup-active")]
        public bool QuiqupActive { get; set; }
        [Column("aramex-active")]
        public bool AramexActive { get; set; }
        [Column("naqel-active")]
        public bool NaqelActive { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
