﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("telescope_entries_tags")]
    public partial class TelescopeEntriesTags
    {
        [Column("entry_uuid")]
        public Guid EntryUuid { get; set; }
        [Required]
        [Column("tag", TypeName = "varchar(255)")]
        public string Tag { get; set; }

        public virtual TelescopeEntries EntryUu { get; set; }
    }
}
