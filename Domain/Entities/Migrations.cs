﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("migrations")]
    public partial class Migrations
    {
        [Key]
        [Column("id")]
        public uint Id { get; set; }
        [Required]
        [Column("migration", TypeName = "varchar(255)")]
        public string Migration { get; set; }
        [Column("batch")]
        public int Batch { get; set; }
    }
}
