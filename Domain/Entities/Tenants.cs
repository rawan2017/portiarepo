﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("tenants")]
    public partial class Tenants
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("vetrina_tenant_id", TypeName = "varchar(200)")]
        public string VetrinaTenantId { get; set; }
        [Column("organization_id")]
        public long? OrganizationId { get; set; }
        [Required]
        [Column("name", TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column("country", TypeName = "varchar(255)")]
        public string Country { get; set; }
        [Required]
        [Column("host", TypeName = "varchar(300)")]
        public string Host { get; set; }
        [Column("owner_info", TypeName = "longtext")]
        public string OwnerInfo { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
    }
}
