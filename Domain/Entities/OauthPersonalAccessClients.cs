﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("oauth_personal_access_clients")]
    public partial class OauthPersonalAccessClients
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("client_id")]
        public ulong ClientId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
