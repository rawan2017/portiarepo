﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("orders")]
    public partial class Orders
    {
        [Key]
        [Column("portia_id")]
        public ulong PortiaId { get; set; }
        [Required]
        [Column("vetrina_id", TypeName = "varchar(200)")]
        public string VetrinaId { get; set; }
        [Column("amount", TypeName = "decimal(12,3)")]
        public decimal Amount { get; set; }
        [Required]
        [Column("currency", TypeName = "varchar(5)")]
        public string Currency { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("completed_at", TypeName = "datetime")]
        public DateTime? CompletedAt { get; set; }
        [Column("shipment_date", TypeName = "timestamp")]
        public DateTime? ShipmentDate { get; set; }
        [Column("delivery_date", TypeName = "timestamp")]
        public DateTime? DeliveryDate { get; set; }
        [Required]
        [Column("delivery_method", TypeName = "varchar(255)")]
        public string DeliveryMethod { get; set; }
        [Column("preparation_time", TypeName = "varchar(255)")]
        public string PreparationTime { get; set; }
        [Column("confirmed")]
        public bool Confirmed { get; set; }
        [Column("canceld")]
        public bool Canceld { get; set; }
        [Column("authorized")]
        public bool Authorized { get; set; }
        [Column("paid")]
        public bool Paid { get; set; }
        [Required]
        [Column("visible")]
        public bool? Visible { get; set; }
        [Required]
        [Column("fullfilment", TypeName = "varchar(5)")]
        public string Fullfilment { get; set; }
        [Column("customer_name", TypeName = "varchar(256)")]
        public string CustomerName { get; set; }
        [Column("customer_ip", TypeName = "varchar(256)")]
        public string CustomerIp { get; set; }
        [Column("customer_phone", TypeName = "varchar(256)")]
        public string CustomerPhone { get; set; }
        [Column("customer_email", TypeName = "varchar(256)")]
        public string CustomerEmail { get; set; }
        [Column("billing_address", TypeName = "text")]
        public string BillingAddress { get; set; }
        [Column("shipping_address", TypeName = "text")]
        public string ShippingAddress { get; set; }
        [Column("custom_fields", TypeName = "text")]
        public string CustomFields { get; set; }
        [Column("purches_total", TypeName = "decimal(12,3)")]
        public decimal? PurchesTotal { get; set; }
        [Column("adjustments_total", TypeName = "decimal(12,3)")]
        public decimal? AdjustmentsTotal { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Column("source", TypeName = "varchar(32)")]
        public string Source { get; set; }
        [Column("brief_en", TypeName = "text")]
        public string BriefEn { get; set; }
        [Column("brief_ar", TypeName = "text")]
        public string BriefAr { get; set; }
        [Column("payment_method", TypeName = "varchar(255)")]
        public string PaymentMethod { get; set; }
        [Column("invoice_links", TypeName = "text")]
        public string InvoiceLinks { get; set; }
        [Column("invoice_number")]
        public uint? InvoiceNumber { get; set; }
        [Column("receipt_links", TypeName = "text")]
        public string ReceiptLinks { get; set; }
        [Column("is_dummy")]
        public sbyte IsDummy { get; set; }
        [Column("cancel_resoan", TypeName = "text")]
        public string CancelResoan { get; set; }
    }
}
