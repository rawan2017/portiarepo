﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("jobs")]
    public partial class Jobs
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("queue", TypeName = "varchar(255)")]
        public string Queue { get; set; }
        [Required]
        [Column("payload", TypeName = "longtext")]
        public string Payload { get; set; }
        [Column("attempts")]
        public byte Attempts { get; set; }
        [Column("reserved_at")]
        public uint? ReservedAt { get; set; }
        [Column("available_at")]
        public uint AvailableAt { get; set; }
        [Column("created_at")]
        public uint CreatedAt { get; set; }
    }
}
