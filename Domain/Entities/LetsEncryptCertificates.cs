﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("lets_encrypt_certificates")]
    public partial class LetsEncryptCertificates
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("deleted_at", TypeName = "timestamp")]
        public DateTime? DeletedAt { get; set; }
        [Required]
        [Column("domain", TypeName = "varchar(255)")]
        public string Domain { get; set; }
        [Column("last_renewed_at", TypeName = "timestamp")]
        public DateTime? LastRenewedAt { get; set; }
        [Column("created")]
        public bool Created { get; set; }
        [Column("fullchain_path", TypeName = "varchar(255)")]
        public string FullchainPath { get; set; }
        [Column("chain_path", TypeName = "varchar(255)")]
        public string ChainPath { get; set; }
        [Column("cert_path", TypeName = "varchar(255)")]
        public string CertPath { get; set; }
        [Column("privkey_path", TypeName = "varchar(255)")]
        public string PrivkeyPath { get; set; }
        [Column("certificate_id", TypeName = "varchar(255)")]
        public string CertificateId { get; set; }
        [Column("privatekey_id", TypeName = "varchar(255)")]
        public string PrivatekeyId { get; set; }
        [Column("domain_ip", TypeName = "varchar(255)")]
        public string DomainIp { get; set; }
    }
}
