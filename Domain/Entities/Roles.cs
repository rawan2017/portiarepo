﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("roles")]
    public partial class Roles
    {
        public Roles()
        {
            RolePermission = new HashSet<RolePermission>();
            RoleUser = new HashSet<RoleUser>();
        }

        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("slug", TypeName = "varchar(255)")]
        public string Slug { get; set; }
        [Column("name", TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<RolePermission> RolePermission { get; set; }
        [InverseProperty("Role")]
        public virtual ICollection<RoleUser> RoleUser { get; set; }
    }
}
