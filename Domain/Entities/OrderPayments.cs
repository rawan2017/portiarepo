﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("order_payments")]
    public partial class OrderPayments
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("portia_order_id")]
        public long PortiaOrderId { get; set; }
        [Required]
        [Column("vetrina_order_id", TypeName = "varchar(200)")]
        public string VetrinaOrderId { get; set; }
        [Column("method_name", TypeName = "varchar(200)")]
        public string MethodName { get; set; }
        [Column("order_total", TypeName = "decimal(12,3)")]
        public decimal OrderTotal { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("transaction_data", TypeName = "text")]
        public string TransactionData { get; set; }
        [Column("payment_amount", TypeName = "decimal(12,3)")]
        public decimal PaymentAmount { get; set; }
        [Column("currency", TypeName = "varchar(5)")]
        public string Currency { get; set; }
        [Column("type", TypeName = "varchar(200)")]
        public string Type { get; set; }
    }
}
