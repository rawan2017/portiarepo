﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("order_adjustments")]
    public partial class OrderAdjustments
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("name_ar", TypeName = "text")]
        public string NameAr { get; set; }
        [Column("name_en", TypeName = "text")]
        public string NameEn { get; set; }
        [Column("total", TypeName = "decimal(12,3)")]
        public decimal Total { get; set; }
        [Column("order_id")]
        public int OrderId { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Required]
        [Column("type", TypeName = "varchar(200)")]
        public string Type { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
