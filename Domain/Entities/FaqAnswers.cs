﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("faq_answers")]
    public partial class FaqAnswers
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("question_id")]
        public ulong QuestionId { get; set; }
        [Required]
        [Column("body_ar", TypeName = "text")]
        public string BodyAr { get; set; }
        [Required]
        [Column("body_en", TypeName = "text")]
        public string BodyEn { get; set; }
        [Required]
        [Column("images_ar", TypeName = "text")]
        public string ImagesAr { get; set; }
        [Required]
        [Column("images_en", TypeName = "text")]
        public string ImagesEn { get; set; }
        [Column("order")]
        public uint Order { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
