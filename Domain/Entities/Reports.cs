﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("reports")]
    public partial class Reports
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Required]
        [Column("name_en", TypeName = "varchar(255)")]
        public string NameEn { get; set; }
        [Column("name_ar", TypeName = "varchar(255)")]
        public string NameAr { get; set; }
        [Column("status_label_en", TypeName = "varchar(255)")]
        public string StatusLabelEn { get; set; }
        [Column("status_label_ar", TypeName = "varchar(255)")]
        public string StatusLabelAr { get; set; }
        [Column("status_value_en", TypeName = "varchar(255)")]
        public string StatusValueEn { get; set; }
        [Column("status_value_ar", TypeName = "varchar(255)")]
        public string StatusValueAr { get; set; }
        [Required]
        [Column("generating_class", TypeName = "varchar(255)")]
        public string GeneratingClass { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
    }
}
