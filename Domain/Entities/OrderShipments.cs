﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("order_shipments")]
    public partial class OrderShipments
    {
        [Key]
        [Column("id")]
        public ulong Id { get; set; }
        [Column("portia_order_id")]
        public long PortiaOrderId { get; set; }
        [Required]
        [Column("vetrina_ship_id", TypeName = "varchar(200)")]
        public string VetrinaShipId { get; set; }
        [Required]
        [Column("vetrina_order_id", TypeName = "varchar(200)")]
        public string VetrinaOrderId { get; set; }
        [Column("carrier", TypeName = "varchar(200)")]
        public string Carrier { get; set; }
        [Column("handler", TypeName = "varchar(200)")]
        public string Handler { get; set; }
        [Column("handler_shipment_id", TypeName = "varchar(255)")]
        public string HandlerShipmentId { get; set; }
        [Column("handler_pickup_id", TypeName = "varchar(255)")]
        public string HandlerPickupId { get; set; }
        [Column("state", TypeName = "varchar(200)")]
        public string State { get; set; }
        [Column("service", TypeName = "varchar(200)")]
        public string Service { get; set; }
        [Column("tenant_id")]
        public int? TenantId { get; set; }
        [Column("expires_at", TypeName = "datetime")]
        public DateTime? ExpiresAt { get; set; }
        [Column("created_at", TypeName = "timestamp")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "timestamp")]
        public DateTime? UpdatedAt { get; set; }
        [Column("target_delivery", TypeName = "timestamp")]
        public DateTime? TargetDelivery { get; set; }
        [Column("driver_info", TypeName = "longtext")]
        public string DriverInfo { get; set; }
        [Column("transaction_data", TypeName = "longtext")]
        public string TransactionData { get; set; }
        [Column("source", TypeName = "text")]
        public string Source { get; set; }
        [Column("destination", TypeName = "text")]
        public string Destination { get; set; }
        [Column("meta_data", TypeName = "text")]
        public string MetaData { get; set; }
    }
}
