﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public class Enums
    {
        public enum DeliveryCompany
        {
            Aramex = 0,
            Lyve=1,
        }
        public enum Region
        {
            IgnoreRegion = 0,
            KSA = 1,
            UAE = 2
        }
}
}