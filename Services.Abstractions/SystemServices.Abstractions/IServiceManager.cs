﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions.SystemServices.Abstractions
{
    public interface IServiceManager
    {
        IUserService UserService { get; }

    }
}
