﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.SystemServices.Abstractions
{
    public interface IUserService
    {
        Task<UserDto[]> GetUsers();

    }
}
