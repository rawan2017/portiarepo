﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.DeliveryCompanyServices.Abstractions
{
    public interface IDeliveryCompanyService
    {
        Task<double> GetCalculationRate();

    }
}
