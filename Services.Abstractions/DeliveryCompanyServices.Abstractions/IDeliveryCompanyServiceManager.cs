﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions.DeliveryCompanyServices.Abstractions
{
    public interface IDeliveryCompanyServiceManager
    {
        IDictionary<Enum,IDeliveryCompanyService> AramexServices { get; }
        IDeliveryCompanyService LyveService { get; }

    }
}
