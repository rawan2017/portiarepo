﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions.DeliveryCompanyServices.Abstractions
{
   public class IAramexServiceManager
    {
        IDeliveryCompanyService AramexService { get; }
        IDeliveryCompanyService KSAService { get; }
        IDeliveryCompanyService UAEService { get; }
    }
}
